var util = require('util');
var Syslog = require('node-syslog');

var level = 5;
var env = process.env.NODE_ENV;

if (env != 'production') {
    var log = Syslog.log;
    Syslog.log = function(level, message) {
        log.call(Syslog, level, message);
        console.log(new Date() + ' ' + message);
    };
}

var Logger = module.exports = {
    info: function(message) {
        this.write('INFO ', message);
    },

    debug: function(message) {
        this.write('DEBUG', message);
    },

    warn: function(message) {
        this.write('WARN', message);
    },

    error: function(message) {
        this.write('ERROR', message);
    },

    crit: function(message) {
        this.write('CRIT ', message);
    },

    write: function(tag, message) {
        var message = '[' + tag + '] ' + getStackTranceOrInspect(message);

        tag = tag.trim();

        if (tag == 'DEBUG' && level >= Logger.DEBUG) {
            Syslog.log(Syslog.LOG_DEBUG, message);
        } else if (tag == 'INFO' && level >= Logger.INFO) {
            Syslog.log(Syslog.LOG_INFO, message);
        } else if (tag == 'WARN' && level >= Logger.WARN) {
            Syslog.log(Syslog.LOG_WARNING, message);
        } else if (tag == 'ERROR' && level >= Logger.ERROR) {
            Syslog.log(Syslog.LOG_ERR, message);
        } else if (tag == 'CRIT' && level >= Logger.CRIT) {
            Syslog.log(Syslog.LOG_CRIT, message);
        }
    }
};

Logger.init = function(name) {
    Syslog.init(name, Syslog.LOG_PID | Syslog.LOG_ODELAY, Syslog.LOG_LOCAL5);
    return Logger;
};

Logger.__defineSetter__('level', function(value) {
    level = value;
});

Logger.CRIT = 1;
Logger.ERROR = 2;
Logger.WARN = 3;
Logger.INFO = 4;
Logger.DEBUG = 5;

function getStackTranceOrInspect(message) {
    if (message.stack) {
        return message.stack;
    } else {
        return inspect(message);
    }
}

function inspect(message) {
    if (message instanceof Object) {
        message = util.inspect(message);
    }
    return message;
}

process.on('uncaughtException', function(error) {
    Logger.crit(error);
});
